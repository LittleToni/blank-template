<?php
    defined( '_JEXEC' ) or die;

    /**
     * Joomla Blank Template - Library v1.0.0
     * https://gitlab.com/LittleToni
     * Copyright 2019, Toni Patke <toni.patke@outlook.de>
     */

    $app = JFactory::getApplication();
    $doc = JFactory::getDocument();
    $menu = $app->getMenu();

    $templatePath = $this->baseurl . 'templates/' . $this->template;
    $rootPath = $_SERVER['DOCUMENT_ROOT'] . '/templates/' . $this->template;

    /**
     * Set default body css-class. 
     * Extend class if active page is default page.
     */
    $bodyClass = 'body';

    if ($menu->getActive() == $menu->getDefault()) {
        $bodyClass .= ' is-frontpage';
    }

    /**
     * Get used language and language direction.
     */
    $lang = JFactory::getLanguage();
    $direction = $lang->get('rtl');

    if ($direction == 0) {
        $direction = 'ltr';
    } else {
        $direction = 'rtl';
    }

    /**
     * Unset Joomla default script and style Resources to prevent
     * conflicts between different Libraries
     */

    foreach ($doc->_styleSheets as $sheet => $settings) {
        unset($doc->_styleSheets[$sheet]);
    }

    foreach ($doc->_scripts as $script => $settings) {
        unset($doc->_scripts[$script]);
    }

    /**
     * Unset Joomla default inline scripts and styles. Define empty array's
     * to prevent errors when render the head by joomla.
     */

    unset($doc->_script);
    unset($doc->_style);

    $doc->_script = [];
    $doc->_style = [];