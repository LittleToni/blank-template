<?php
    defined('_JEXEC') or die;

    include_once JPATH_THEMES . '/' . $this->template . '/libraries/template.inc.php';
?>

<!DOCTYPE html>

<html dir="<?php echo $direction; ?>" lang="<?php echo $this->language; ?>">
    <head>
        <jdoc:include type="head" />

        <?php include_once 'inc/meta-data.php'; ?>
        <?php include_once 'inc/touch-icon.php'; ?>

        <link rel="stylesheet" href="css/template.css">
    </head>

    <body class="<?php echo $bodyClass; ?>">
        <jdoc:include type="modules" name="fixed" />
        <jdoc:include type="modules" name="top" />

        <main>
            <jdoc:include type="message" />
            <jdoc:include type="component" />
        </main>

        <jdoc:include type="modules" name="bottom" />

        <script src="js/template.js"></script>
    </body>
</html>
